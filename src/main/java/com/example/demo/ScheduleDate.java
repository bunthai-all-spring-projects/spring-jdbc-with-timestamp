package com.example.demo;

import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

/**
 * Value Object for scheduleDate
 * @author congnh
 */
@EqualsAndHashCode
public class ScheduleDate
{
    private static final long VALID_TIME_RANGE_SECOND = 300;

//    private static final String UTC_DATETIME_PATTERN = "EEEE,yyyy-MM-dd'T'HH:mm:ssXXX";

//    private static final DateTimeFormatter UTC_DATETIME_FORMATTER = DateTimeFormatter.ofPattern(UTC_DATETIME_PATTERN);

    private OffsetDateTime timeStamp;

    private ScheduleDate(@NonNull OffsetDateTime _timeStamp)
    {
//        if (_timeStamp.toEpochSecond() % VALID_TIME_RANGE_SECOND != 0) {
//            throw new IllegalArgumentException("scheduleDate must be round of 5 minutes");
//        }
        this.timeStamp = _timeStamp;
    }

    /**
     * Check whether current <code>ScheduleDate</code> is after specific <code>_dateTime</code>
     * @param _dateTime
     * @return true if <code>ScheduleDate</code> is after(inclusive) _dateTime, false otherwise
     */
    public boolean isAfter(OffsetDateTime _dateTime)
    {
        return timeStamp.isEqual(_dateTime) || timeStamp.isAfter(_dateTime);
    }

    public static ScheduleDate of(@NonNull String _value)
    {
        try {
            return new ScheduleDate(OffsetDateTime.parse(_value).withOffsetSameInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS));
        } catch (DateTimeParseException _e) {
            throw new IllegalArgumentException(_e);
        }
    }

    public static ScheduleDate of(@NonNull Timestamp _timestamp)
    {
        return new ScheduleDate(OffsetDateTime.ofInstant(Instant.ofEpochMilli(_timestamp.getTime()), ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS));
    }

    public static ScheduleDate of(@NonNull OffsetDateTime _timestamp)
    {
        return new ScheduleDate(_timestamp.atZoneSameInstant(ZoneOffset.UTC).toOffsetDateTime());
    }

    public Timestamp toTimestamp()
    {
        return Timestamp.from(this.timeStamp.withOffsetSameInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).toInstant());
    }

    public OffsetDateTime toOffsetDateTime()
    {
        return this.timeStamp;
    }

    @Override
    public String toString()
    {
//        return UTC_DATETIME_FORMATTER.format(this.timeStamp);
        return String.valueOf(this.timeStamp);
    }
}
