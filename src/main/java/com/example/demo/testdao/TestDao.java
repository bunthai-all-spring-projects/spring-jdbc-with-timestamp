package com.example.demo.testdao;

import com.example.demo.ScheduleDate;
import com.example.demo.SpiralTimeStamp;
import com.example.demo.models.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class TestDao {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    public Test insert(Test test) {
        String sql = "INSERT INTO test(type, repeat_period_timestamp, schedule_dates, period) " +
                "VALUES(:type, :repeat_period_timestamp, :schedule_dates::TIMESTAMPTZ[], :period::JSONB)" +
                "RETURNING *";

        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("type", test.getType());
        param.addValue("repeat_period_timestamp", test.getRepeatPeriodTimeStamp().toTimestamp());
        param.addValue("schedule_dates", test.getScheduleDatesToPostgresArray() );
        try {
            param.addValue("period", new ObjectMapper().writeValueAsString(test.getPeriod()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jdbcTemplate.queryForObject(sql, param, Test.rowMapper());
    }

    public List<Test> findAll() {
        String sql = "SELECT * FROM test";
        return jdbcTemplate.query(sql, Test.rowMapper());
    }

    public Optional<Test> findByTestId(long id) {
        String sql = "SELECT * from test WHERE id = :id";
        MapSqlParameterSource param =  new MapSqlParameterSource();
        param.addValue("id", id);
        try {
            return Optional.of(jdbcTemplate.queryForObject(sql, param, Test.rowMapper()));
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return Optional.empty();
        }
    }


}
