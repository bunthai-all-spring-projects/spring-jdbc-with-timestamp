package com.example.demo;

import com.example.demo.models.Test;
import com.example.demo.testdao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {


	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);

//		System.out.println(ScheduleDate.of("2019-08-21T18:50:00Z"));

		OffsetDateTime offsetDateTime = OffsetDateTime.parse("2019-08-22T01:50:00Z");
		System.out.println(offsetDateTime);
		System.out.println(Timestamp.from(offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).toInstant()));
		System.out.println(OffsetDateTime.ofInstant(Instant.ofEpochMilli(Timestamp.from(offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).toInstant()).getTime()), ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS));

//		OffsetDateTime offsetDateTime = OffsetDateTime.now();
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH mm");
//		System.out.println(dtf.format(offsetDateTime));
//		System.out.println(dtf.format(offsetDateTime));
//		System.out.println(offsetDateTime);
//		System.out.println(offsetDateTime.withOffsetSameInstant(ZoneOffset.MIN));
//		System.out.println(offsetDateTime.withOffsetSameInstant(ZoneOffset.MAX));
//		System.out.println(offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC));

	}


	@Autowired
	TestDao testDao;

	@Override
	public void run(String... args) throws Exception {

		Map<String, String> period = new HashMap<>();
//		period.put("year", "2019");
//		period.put("month", "10");
//		period.put("day", "2");
		period.put("time", "20:00");
//		period.put("wday", "Tue");
//		period.put("time", "20:00");
//		SpiralTimeStamp repeat_period_timestamp = SpiralTimeStamp.of("2019-08-31T01:50:00Z");
		SpiralTimeStamp repeat_period_timestamp = SpiralTimeStamp.of("2019-09-14T01:50:00Z");
		Test test = new Test();//.toBuilder().type("monthly").period(period).repeatPeriodTimeStamp(repeat_period_timestamp).build();
		test.setPeriod(period);
//		test.setType("weekly");
		test.setType("daily");
		test.setRepeatPeriodTimeStamp(repeat_period_timestamp);
		test.getScheduleDates().add(SpiralTimeStamp.of("2019-08-22T01:50:00Z"));
		test.getScheduleDates().add(SpiralTimeStamp.of("2019-08-22T01:50:00Z"));
		test.setPeriod(period);

		this.testDao.insert(test);
		System.out.println(this.testDao.findAll());
		System.out.println(this.testDao.findByTestId(73).isPresent() ? this.testDao.findByTestId(73).get() : "none");

	}
}
