package com.example.demo.models;

import com.example.demo.SpiralTimeStamp;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.jackson.JsonObjectSerializer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.sql.Array;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
public class Test {
    private long id;
    private String type;
    private SpiralTimeStamp repeatPeriodTimeStamp;
    private Map<String, String> period;
//    private String day;
//    private String time;
    private List<SpiralTimeStamp> scheduleDates;

    public Test() {
        this.period = new HashMap<>();
        this.scheduleDates = new ArrayList<>();
    }

    public String getScheduleDatesToPostgresArray() {
        if (this.scheduleDates.size() <= 0) {
            return null;
        }

        StringBuilder builder = new StringBuilder();
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.scheduleDates.stream().map(s -> String.valueOf(s.toTimestamp())).collect(Collectors.joining(",")));
        sb.append("}");
        return sb.toString();
    }

    public static RowMapper<Test> rowMapper()
    {
        return (resultSet, ow) -> {
            Test test = new Test();
            test.setId(resultSet.getLong("id"));
            test.setType(resultSet.getString("type"));
            test.setRepeatPeriodTimeStamp(SpiralTimeStamp.of(OffsetDateTime.ofInstant(Instant.ofEpochMilli(resultSet.getTimestamp("repeat_period_timestamp").getTime()), ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS)));
            Array a = resultSet.getArray("schedule_dates");
            List<Timestamp> lstime = Arrays.asList((Timestamp[]) a.getArray());
            test.getScheduleDates().addAll(lstime.stream().map(timestamp -> SpiralTimeStamp.of(OffsetDateTime.ofInstant(Instant.ofEpochMilli(timestamp.getTime()), ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS))).collect(Collectors.toList()));
            try {
                test.setPeriod(new ObjectMapper().readValue(resultSet.getObject("period").toString(), HashMap.class));
//                Map<String, String> period = new ObjectMapper().readValue(resultSet.getObject("period").toString(), HashMap.class);
            } catch (IOException e) {
                e.printStackTrace();
            }


            return test;
        };
    }

    @Override
    public String toString() {
        return "" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", repeatPeriodTimeStamp=" + repeatPeriodTimeStamp +
                ", period=" + period +
                ", scheduleDates=" + scheduleDates +
                "\n";
    }
}
